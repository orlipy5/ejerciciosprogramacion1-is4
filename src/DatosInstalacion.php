<?php
$nombreServidor = $_SERVER['SERVER_NAME'];
$ipServidor = $_SERVER['SERVER_ADDR'];
$softwareServidor = $_SERVER['SERVER_SOFTWARE'];

$versionPHP = phpversion();
$directorioPHP = __DIR__;
$archivoPHP = __FILE__;

echo "Información del Servidor:\n";
echo "Nombre del servidor: $nombreServidor\n";
echo "Dirección IP del servidor: $ipServidor\n";
echo "Software del servidor: $softwareServidor\n";

echo "\nInformación de PHP:\n";
echo "Versión de PHP: $versionPHP\n";
echo "Directorio del archivo PHP actual: $directorioPHP\n";
echo "Archivo PHP actual: $archivoPHP\n";
?>