<?php
$productos = array(
    array('Coca Cola', 100, '4,500'),
    array('Pepsi', 30, '4,800'),
    array('Sprite', 20, '4,500'),
    array('Guaraná', 200, '4.500'),
    array('SevenUp', 24, '4.800'),
    array('Mirinda Naranja', 56, '4.800'),
    array('Mirinda Guaraná', 89, '4.800'),
    array('Fantan Naranja', 10, '4.500'),
    array('Fanta Piña', 2, '4.500'),
);
?>

<!DOCTYPE html>
<html>
<head>
    <style>
        table {
            border-collapse: collapse;
            width: 50%;
            margin: 0 auto;
        }

        th {
            background-color: #fbff0e;
            font-weight: bold;
            text-align: center;
            padding: 10px;
            border: 1px solid black;
        }

        td {
            text-align: center;
            padding: 8px;
            border: 1px solid black;
        }

        tr:nth-child(2) th {
            background-color: #d0cece !important;
        }

        tr:nth-child(even) td {
            background-color: white;
        }

        tr:nth-child(odd) td {
            background-color: #e2efda;
        }
    </style>
</head>
<body>
<?php
echo '<table>';
echo '<tr>';
echo '<th colspan="3">Productos</th>';
echo '</tr>';
echo '<tr>';
echo '<th>Nombre</th>';
echo '<th>Cantidad</th>';
echo '<th>Precio (Gs)</th>';
echo '</tr>';

foreach ($productos as $producto) {
    echo '<tr>';
    echo "<td>{$producto[0]}</td>";
    echo "<td>{$producto[1]}</td>";
    echo "<td>{$producto[2]}</td>";
    echo '</tr>';
}

echo '</table>';
?>
</body>
</html>
